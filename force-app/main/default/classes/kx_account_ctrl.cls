/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name kx_account_ctrl
* @Author Javier Suárez
*/
public with sharing class kx_account_ctrl {

	/**
	* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	* @Name getAccountInfo
	* @Author Javier Suárez
	*/
	@SuppressWarnings('PMD.ExcessivePublicCount, PMD.CyclomaticComplexity, PMD.TooManyFields')
	// MOTIVO DE SUPRESION
	@AuraEnabled(cacheable=true)
	public static kx_account_info_wrapper getAccountInfo(String acctId) {
		// NO TIENE SENTIDO, SOLO COMO EJEMPLO
		Account a = [select Id from Account where id = :acctId];
		System.Debug(a.Id);
		
		return kx_account_service.getAccountInfoById(acctId);
	}
    
}