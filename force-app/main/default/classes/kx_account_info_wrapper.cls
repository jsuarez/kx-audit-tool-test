/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name kx_account_info_wrapper
* @Author Javier Suárez
*/
public class kx_account_info_wrapper {

	@AuraEnabled
	public String name {get; set;}

	@AuraEnabled
	public List<Opportunity> optys {get; set;}
    
}