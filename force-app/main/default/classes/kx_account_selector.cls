/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name kx_account_selector
* @Author Javier Suárez
*/
public with sharing class kx_account_selector {

	/**
	* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	* @Name getAccountById
	* @Author Javier Suárez
	*/
	public static List<Account> getAccountById(String acctId){
		if ( !Schema.sObjectType.Account.fields.Name.isAccessible() ||
             !Schema.sObjectType.Account.fields.Id.isAccessible() ) {
                 
			throw new kx_exception('NO ACCESS');
      }
		return [SELECT Id, Name FROM Account WHERE Id = :acctId];    
	}

}