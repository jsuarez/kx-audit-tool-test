/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name kx_account_service
* @Author Javier Suárez
*/
public class kx_account_service {

	/**
	* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	* @Name getAccountInfoById
	* @Author Javier Suárez
	*/
	public static kx_account_info_wrapper getAccountInfoById(String acctId) {
		List<Account> accountList = kx_account_selector.getAccountById(acctId);

		kx_account_info_wrapper ainfo = new kx_account_info_wrapper();

		if ( accountList.size() == 1 ) {
			ainfo.name = accountList.get(0).name;
		}

		ainfo.optys = kx_opportunity_selector.getOpportunitiesByAccountId(acctId);

		return ainfo;
	}

}