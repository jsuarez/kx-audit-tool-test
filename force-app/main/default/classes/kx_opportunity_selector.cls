/**
* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
* @Name kx_opportunity_selector
* @Author Javier Suárez
*/
public class kx_opportunity_selector {

	/**
	* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
	* @Name getOpportunitiesByAccountId
	* @Author Javier Suárez
	*/
	public static List<Opportunity> getOpportunitiesByAccountId(String acctId){
		if ( !Schema.sObjectType.Opportunity.fields.Name.isAccessible() ||
             !Schema.sObjectType.Opportunity.fields.Id.isAccessible() ||
             !Schema.sObjectType.Opportunity.fields.Amount.isAccessible() ) {
                 
			throw new kx_exception('NO ACCESS');
      }
		return [SELECT Id, Name, Amount FROM Opportunity WHERE AccountId = :acctId];    
	}

}