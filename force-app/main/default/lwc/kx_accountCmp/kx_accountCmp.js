import { LightningElement, wire, api } from 'lwc';
import getAccountInfo from '@salesforce/apex/kx_account_ctrl.getAccountInfo';

export default class Kx_accountCmp extends LightningElement {
	@api recordId;
	@wire(getAccountInfo, { acctId: '$recordId' }) accountInfo;

	handleClick() {
		console.log(this.accountInfo);
	}
}